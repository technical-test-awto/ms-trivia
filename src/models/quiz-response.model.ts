export interface IQuizResponse {
  
  category: string;
  type: string;
  difficulty: string;
  question: string;
  correctAnswer: string;
  answers: string[];
}
