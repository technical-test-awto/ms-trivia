import { IsNotEmpty, IsString } from "class-validator";

export type Difficulty = 'easy' | 'medium' | 'hard' | 'inferno';

export class DifficultyOptions {

  @IsNotEmpty()
  @IsString()
  public difficulty: Difficulty;
}