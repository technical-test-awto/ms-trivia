import { HttpModule } from "@nestjs/axios";
import { Module } from "@nestjs/common";
import { QuizController } from "./controllers/quiz.controller";
import { QuizService } from "./services/quiz.service";

@Module({
  imports:[
    HttpModule
  ],
  providers:[
    QuizService
  ],
  controllers:[
    QuizController
  ]
})

export class QuizModule {};