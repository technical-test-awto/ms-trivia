import { Controller, Get, Query } from '@nestjs/common';
import { DifficultyOptions } from 'src/models/difficulty-options.model';
import { IQuizResponse } from 'src/models/quiz-response.model';
import { QuizService } from '../services/quiz.service';

@Controller('quizzes')
export class QuizController {
  constructor (
    private quizService: QuizService
  ) {}

  @Get()
  getQuiz(
    @Query() {difficulty}: DifficultyOptions,
  ): Promise<IQuizResponse[]> { 
    return this.quizService.getQuizzes(difficulty); 
  }
}
