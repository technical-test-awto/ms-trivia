import { HttpService } from '@nestjs/axios';
import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { lastValueFrom } from 'rxjs';
import { IQuizResponse } from 'src/models/quiz-response.model';
import { quizOptions } from 'src/utils/quiz-options.utils';

@Injectable()
export class QuizService {
  constructor(
    private readonly httpService: HttpService
  ) { }

  public async getQuizzes(option: string): Promise<IQuizResponse[]> {
    const excecutedPenddings = quizOptions[option].map(async (mode) => (await this.excecuteExternalAPI(mode)).data);
    const quizzes = await Promise.allSettled(excecutedPenddings);
    return this.createResponse(quizzes);
  }

  private async excecuteExternalAPI(mode): Promise<AxiosResponse> {
    try {
      const response = this.httpService.get(
        `https://opentdb.com/api.php?amount=${mode.amount}&difficulty=${mode.difficulty}`,
      );
      return lastValueFrom(response);
    } catch (error) {
      throw new InternalServerErrorException;
      ;
    }
  }

  private createResponse(quizzes: any[]): IQuizResponse[] {
    const quizzesFlated = quizzes.map((result) => result.value.results).flat();

    return quizzesFlated.map(({
      category, type, difficulty, question, correct_answer, incorrect_answers
    }) => ({
      category,
      type,
      difficulty,
      question,
      correctAnswer: correct_answer,
      answers: [...incorrect_answers, correct_answer].sort(() => Math.random() - 0.5)
    }));
  }
}



