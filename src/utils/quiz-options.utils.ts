import { QuizDifficulty } from "./enum/quiz-difficulty.enum";

export const quizOptions = {
  
  [QuizDifficulty.EASY]: [
    {
      difficulty: QuizDifficulty.EASY,
      amount: 30,
    },
  ],
  [QuizDifficulty.MEDIUM]: [
    {
      difficulty: QuizDifficulty.EASY,
      amount: 15,
    },
    {
      difficulty: QuizDifficulty.MEDIUM,
      amount: 25,
    },
  ],
  [QuizDifficulty.HARD]: [
    {
      difficulty: QuizDifficulty.MEDIUM,
      amount: 30,
    },
    {
      difficulty: QuizDifficulty.HARD,
      amount: 30,
    },
  ],
  [QuizDifficulty.INFERNO]: [
    {
      difficulty: QuizDifficulty.HARD,
      amount: 70,
    },
  ],
}